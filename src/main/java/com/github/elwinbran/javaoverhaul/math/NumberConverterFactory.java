/*
 * Copyright (c) 2017 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.math;

import com.github.elwinbran.javaoverhaul.numbers.PositiveNumber;
import com.github.elwinbran.javaoverhaul.numbers.WholeNumber;
import com.github.elwinbran.javaoverhaul.numbers.RealNumber;
import com.github.elwinbran.javaoverhaul.numbers.ComplexNumber;
import com.github.elwinbran.javaoverhaul.functional.Function;

/**
 * Provides objects that can convert numbers to a 'richer' number type.
 * So this factory does not provide conversions from complex to positive 
 * number for example.
 * 
 * @author Elwin Slokker
 */
public interface NumberConverterFactory
{
    /**
     * @return a function that converts a positive number to a whole number.
     */
    public abstract Function<PositiveNumber, WholeNumber> toWholeConverter();
    
    /**
     * @return a function that converts a whole number to a real number.
     */
    public abstract Function<WholeNumber, RealNumber> toRealConverter();
    
    /**
     * @return a function that converts a real number to a complex number.
     */
    public abstract Function<RealNumber, ComplexNumber> toCompledConverter();
}
